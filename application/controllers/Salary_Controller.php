<?php
    class Salary_controller extends CI_Controller {

        // construct function
        public function __construct()
        {
            parent:: __construct();
            $this->load->library('form_validation');
            $this->load->model('user_salary_model');
            $this->load->library('session');
            $this->load->helper('tax_payable');
        }

        public function fetch_data()
        {
            $con = array( 
                    'tax_id'=> $this->input->post('tax_id'),  
            ); 
            $data = $this->user_salary_model->get_rows($con, 'user_salary', 'tax_id'); 
            echo json_encode($data);
        }

        // function to find out salary , tax paid and expected tax for a given time period of a year
        public function expected_tax(){
            // getting values from the url and storing them into variables
            $id = $_GET['id'];
            $start = $_GET['start_year'].'-07-01';
            $end = $_GET['end_year'].'-06-30';

            // getting sum of tax payable amount for the given time period
            $salary_received = $this->user_salary_model->extract_yearly_record($start, $end, $id , 'tax_payable_amount');
            // total salary month records this year by sending column name empty in parameters
            $total_records = $this->user_salary_model->extract_yearly_record($start, $end, $id , '');
            $remaining_months = 12 - $total_records;
            // last month salary record by sending last row in parameter, as remaining month salary will be predicted on latest record
            $last_salary = $this->user_salary_model->extract_yearly_record($start, $end, $id , 'last_row');
            $salary_remaining = $total_records>0 ? ($last_salary->tax_payable_amount) * $remaining_months : 0;
            $year_salary = $salary_received + $salary_remaining;
            
            // storing results in array and sending them back in json form
            $result['taxable_amount'] = $total_records>0 ? $salary_received :0;
            $result['tax_paid'] = $total_records>0 ? $this->user_salary_model->extract_yearly_record($start, $end, $id , 'tax_paid'): 0;
            $result['expected_tax'] = $total_records>0 ? ceil(tax_payable($year_salary)) : 0;
            $result['tax_per_month'] = $total_records>0 ? ceil(($result['expected_tax']-$result['tax_paid'])/$remaining_months) : 0;
           
            echo json_encode($result);

        }

        // method to add or update salary records
        public function add_update_salary(){

            $tax_id = $this->input->post('tax_id');

            $data = array(
                'user_id' => $this->input->post('user_id'),
                'month_year' => $this->input->post('month_year'),
                'gross_salary' => $this->input->post('gross_salary'),
                'bonus' => $this->input->post('bonus'),
                'bonus_type' => $this->input->post('bonus_type'),
                'medical_allowance' => $this->input->post('medical_allowance'),
                'tax_payable_amount' => $this->input->post('gross_salary') + $this->input->post('bonus') - $this->input->post('medical_allowance'),
                'tax_paid' => $this->input->post('tax_paid'),
            );

            if ($tax_id == '') {
                $tax_id = $this->user_salary_model->insert('user_salary',$data);
            } else {
                $this->user_salary_model->update('user_salary', $data, 'tax_id', $tax_id);
            }
            $con = array( 
                    'tax_id'=> $tax_id,  
            ); 
            $data = $this->user_salary_model->get_rows($con, 'user_salary', 'tax_id'); 
            echo json_encode($data);
        }

        
        // delete a record
        public function delete_salary()
        {
            $id = $this->uri->segment('3'); 
            $this->user_salary_model->delete('user_salary' , 'tax_id' , $id); 
        }

        // show salary table
        public function salary_table()
        {
            $salary['userId'] = intval($_GET['id']);
            if(!$salary['userId']){
                $query = $this->db->get('user_salary');
                $data['records'] = $query->result();
            } else {
                $this->db->where('user_id',  $salary['userId']);
                $query = $this->db->get('user_salary');
                $data['records'] = $query->result();
            }
            $this->load->view('users/salary_table', $data);
        }

        // form to add or edit salary record
        public function form_salary()
        {
            $salary['tax_id'] = $this->uri->segment('3');
            if(!$salary['tax_id']){
                $salary['tax_id'] = '';
            }
            $this->load->view('users/add_edit_salary', $salary);           
        }

    }
?>