<?php
    class User_controller extends CI_Controller {

        // construct function
        public function __construct()
        {
            parent:: __construct();
            $this->load->library('form_validation');
            $this->load->model('user_salary_model');
            $this->load->library('session');
            
            $this->isUserLoggedIn = $this->session->userdata('isUserLoggerIn');
        }


        // method for showing login view
        public function login_view()
        {
            $this->load->view('users/login');
        }
        // method for showing register user view
        public function register_view()
        {
            $this->load->view('users/registration');
        }


        // function to redirect user on login page or dashboard if he is login or not 
        public function account(){ 
            $data = array(); 
            if($_SESSION['isUserLoggedIn']){  
                
                $con = array( 
                    'id' =>  $_SESSION['userId']
                );  
                $data['user'] = $this->user_salary_model->get_rows($con, 'user', 'id');
                $data['user_count'] = $this->db->count_all_results('user');
                $data['salary_count'] = $this->db->count_all_results('user_salary');

                if ($_SESSION['user_type'] == 1) {  
                    $query = $this->db->get('user');
                    $data['records'] = $query->result();
                    $this->load->view('users/admin_account', $data);
                } else {
                    $this->db->where('user_id',  $_SESSION['userId']);
                    $query = $this->db->get('user_salary');
                    $data['records'] = $query->result();
                    $this->load->view('users/user_account', $data);
                }     
            } else { 
                redirect('login');
            }

        }


        // login function 
        public function login()
        {
            $data = array(); 
         
        // Get messages from the session 
            if($this->session->userdata('success_msg')){ 
            $data['success_msg'] = $this->session->userdata('success_msg'); 
            $this->session->unset_userdata('success_msg'); 
            } 
            if($this->session->userdata('error_msg')){ 
            $data['error_msg'] = $this->session->userdata('error_msg'); 
            $this->session->unset_userdata('error_msg'); 
            } 
         
        // If login request submitted 
            if($this->input->post('loginSubmit')){ 
                $this->form_validation->set_rules('email', 'email', 'required|valid_email'); 
                $this->form_validation->set_rules('password', 'password', 'required'); 
             
                if($this->form_validation->run() == true){ 
                    $con = array( 
                        'returnType' => 'single', 
                        'conditions' => array( 
                            'email'=> $this->input->post('email'), 
                            'password' => $this->input->post('password'), 
                        )
                    ); 

                    $checkLogin = $this->user_salary_model->get_rows($con, 'user'); 
                    if($checkLogin){ 
                        $this->session->set_userdata('isUserLoggedIn', TRUE); 
                        $this->session->set_userdata('userId', $checkLogin['id']);
                        $this->session->set_userdata('userName', $checkLogin['name']);
                        $this->session->set_userdata('user_type', $checkLogin['user_type']);
                        redirect('profile'); 
                    } else { 
                    $data['error_msg'] = 'Wrong email or password, please try again.'; 
                    } 
                } else { 
                $data['error_msg'] = 'Please fill all the mandatory fields.'; 
                } 
                $this->load->view('users/login', $data);
            }
        }
    // code for registering a user to database
        public function register_user()
        {
            $data = $userData = array(); 
         
        // If registration request is submitted 
            if($this->input->post('signupSubmit')){ 
                $this->form_validation->set_rules('name', 'Full Name', 'required');  
                $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[user.email]');
                $this->form_validation->set_rules('phone', 'Phone Number', 'required'); 
                $this->form_validation->set_rules('password', 'Password', 'required'); 
                $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]'); 
 
                $userData = array( 
                    'name' => strip_tags($this->input->post('name')),  
                    'email' => strip_tags($this->input->post('email')), 
                    'phone' => strip_tags($this->input->post('phone')),
                    'user_type' => 2, 
                    'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT),
                    ); 
 
                if($this->form_validation->run() == true){ 
                    $insert = $this->user_salary_model->insert('user', $userData); 
                    if($insert){ 
                        $this->session->set_userdata('success_msg', 'Your account registration has been successful. Please login to your account.'); 
                        redirect('login');
                    } else { 
                        $data['error_msg'] = 'Some problems occured, please try again.'; 
                    } 
                } else { 
                    $data['error_msg'] = 'Please fill all the mandatory fields.'; 
                } 
            } 
         
        // Posted data 
            $data['user'] = $userData; 
         
        // Load view 
            $this->load->view('users/registration', $data);
        } 
     

        public function logout(){ 
            $this->session->unset_userdata('isUserLoggedIn'); 
            $this->session->unset_userdata('userId'); 
            $this->session->sess_destroy();
            redirect('login'); 
        } 

        // function to show form for adding and updating user 
        public function form_user()
        {
            $user['id'] = $this->uri->segment('3');
            if(!$user['id']){
                $user['id'] = '';
            }
            $this->load->view('users/add_edit_user', $user);           
        }

        // show table of users
        public function user_table()
        {
            $query = $this->db->get('user');
            $data['records'] = $query->result();
            $this->load->view('users/user_table', $data);
        }

        // fetch data for a specific user with his id
        public function fetch_data()
        {
            $con = array( 
                    'id'=> $this->input->post('id'),  
            ); 
            $data = $this->user_salary_model->get_rows($con, 'user', 'id'); 
            echo json_encode($data);
        }


        // delete a user's record
        public function delete_user()
        {
            $id = $this->uri->segment('3'); 
            // delete from user table 
            $this->user_salary_model->delete('user', 'id', $id);
            // delete his salary record also 
            $this->user_salary_model->delete('user_salary', 'user_id', $id);
        }

        // add or update a user record 
        public function update_user()
        {
            $id = $this->input->post('id'); 
			
            $data = array( 
                "name" => $this->input->post("name"),
                "email" => $this->input->post("email"),
                "phone" => $this->input->post("phone"),
                "user_type" => ($this->input->post("user_type") == "admin" ? 1 : 2),
                "password" => password_hash($this->input->post("password"), PASSWORD_BCRYPT),
            ); 
                
            if ($id == '') {
                $id = $this->user_salary_model->insert('user', $data);
            } else {
                $this->user_salary_model->update('user', $data, 'id', $id);
            }     
            // $query = $this->db->get("user"); 
            // $data['records'] = $query->result();
            $con = array( 
                'id'=> $id,  
            ); 
            $data = $this->user_salary_model->get_rows($con, 'user', 'id');
            echo json_encode($data); 
        }

    }

?>