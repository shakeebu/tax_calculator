<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_User extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '50',
                        ),
                        'email' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '50',
                            'unique' => TRUE
                        ),
                        'phone' => array(
                            'type' => 'INT',
                            'constraint' => '20',
                            'unique' => TRUE
                        ),
                        'user_type' => array(
                            'type' => 'INT',
                            'constraint' => '1',
                            'default' => 1,
                        ),
                        'password' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '100',
                ),
                        'image' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                                'null' => TRUE,
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('user');
        }

        public function down()
        {
                $this->dbforge->drop_table('user');
        }
}