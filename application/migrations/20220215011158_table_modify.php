<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Migration_Table_modify extends CI_Migration {

        public function __construct()
        {
            parent::__construct();
            $this->load->dbforge();
        }

        public function up()
        {
            $fields = array(
                'user_type' => array(
                    'type' => 'TINYINT',
                    'constraint' => '5',
                ),
                'id' => array(
                    'name' => 'user_id',
                    'type' => 'INT',
                    'constraint' => 5,
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE
                ),
            );

            $this->dbforge->modify_column('user', $fields);




            $fields = array(
                'id' => array(
                    'name' => 'role_id',
                    'type' => 'INT',
                    'constraint' => 5,
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE
                ),
            );

            $this->dbforge->modify_column('role', $fields);





            $fields = array(
                'id' => array(
                    'name' => 'salary_id',
                    'type' => 'INT',
                    'constraint' => 5,
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE
            ),
                'salary' => array(
                    'name' => 'gross_salary',
                    'type' => 'INT',
                    'constraint' => '10',
                ), 
                'med_all' => array(
                    'name' => 'medical_allowance',
                    'type' => 'INT',
                    'constraint' => '10',
                ),
                'payslip' => array(
                    'name' => 'payslip_image'
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                    'null' => TRUE,
                ),                
            );

            $this->dbforge->modify_column('tax', $fields);
        }

        public function down()
        {
            $this->dbforge->drop_column('user', array('user_type', 'id'));
            $this->dbforge->drop_column('role', 'id');
            $this->dbforge->drop_column('user_salary', array('id', 'salary', 'med_all', 'payslip'));
        }
    }

?>