<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Migration_New_tables extends CI_Migration {

        public function up(){
                
            $this->dbforge->add_field(array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => 5,
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE
                    ),
                'role' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '20',
                    ),
                ));

            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('role');


            $this->dbforge->add_field(array(
                'id' => array(
                        'type' => 'INT',
                        'constraint' => 5,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                ),
                'user_id' => array(
                        'type' => 'INT',
                        'constraint' => '5',
                ),
                'salary' => array(
                        'type' => 'INT',
                        'constraint' => '10',
                ),
                'bonus' => array(
                    'type' => 'INT',
                    'constraint' => '10',
                    'Null' => TRUE,
                ),
                'bonus_type' => array(
                    'type' => 'TEXT',
                    'constraint' => '100',
                    'null' => TRUE,
                ),
                'med_all' => array(
                    'type' => 'INT',
                    'constraint' => '10',
                ),
                'payslip' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '100',
                        'null' => TRUE,
                ),
            ));
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('tax');

            }

            public function down()
            {
                $this->dbforge->drop_table('role');
                $this->dbforge->drop_table('tax');
            }
    }

?>