<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Migration_Modified extends CI_Migration {

        public function __construct()
        {
            parent::__construct();
            $this->load->dbforge();
        }

        public function up()
        {
            $fields = array(
                'user_type' => array(
                    'type' => 'INT',
                    'constraint' => '5',
                    'NULL' => TRUE,
                ),
            );

            $this->dbforge->modify_column('user', $fields);
        }

        public function down()
        {
            $this->dbforge->drop_column('user', 'user_type');
        }
    }

?>