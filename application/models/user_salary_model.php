<?php defined("BASEPATH") OR exit("no direct script access allowed");

    class user_salary_model extends CI_model {

        public function __construct(){

        }


        // function to get rows from database 
        public function get_rows($params = array(), $tableName , $key = '') {

            // get all data of specific table from database
            $this->db->select('*');
            $this->db->from($tableName);

            // check if conditions in params find them in database for each condition
            if (array_key_exists('conditions', $params)) {
                // if there is email in conditions it means user is trying to login
                if (array_key_exists('email', $params['conditions'])) {
                    // get all results where email matched
                    $query = $this->db->where('email', $params['conditions']['email'])->get();
                    // if only one record found this is good news 
                    if ($query->num_rows() === 1) {
                        // get those results in an array and match hashed password,
                        $result = $query->row_array();
                        if (password_verify(strVal($params['conditions']['password']), strVal($result['password']))) {
                            unset($result['password']);
                            return $result;
                        } else{
                            return false;
                        }

                    } else {
                        return false;
                    }
                } else {
                    foreach($params['conditions'] as $key => $value) {
                    $this->db->where($key, $value);
                    }
                }
            }
            // check if the given value for given key exists in table or returntype is single
            if (array_key_exists($key , $params) || $params['returnType'] == 'single') {
                if(!empty($params[$key])){
                    $this->db->where($key, $params[$key]);
                }
            }
            $query = $this->db->get();
            $result = $query->row_array();
            return $result;
        }


        // function to insert data in database with parameters name of the table and data to be inserted in that table
        public function insert($tableName, $data){
                $insert = $this->db->insert($tableName, $data);
                return $insert ? $this->db->insert_id() : false;
        }


        // function to update data in some table with parameters name of table and data to be updated 
        // and name of the primary key and value of primary key where data will be updated
        public function update($tableName, $data, $primaryKey, $value){
            $this->db->set($data);
            $this->db->where($primaryKey, $value);
            $this->db->update($tableName, $data);
        }


        // function to delete data in some tables with parameters name of table and name or primary key and value of primary key
        public function delete($tableName, $primaryKey, $value){
            $this->db->delete($tableName, $primaryKey .'='. $value);
        }


        public function extract_yearly_record($start, $end, $id='', $column){
            
            if ($id != '') {
                $this->db->where('user_id', $id);
            }
            $this->db->where('month_year >=', date_format(date_create($start), 'Y-m-d'));
            $this->db->where('month_year <=', date_format(date_create($end), 'Y-m-d'));

            if ($column == '') {
                $count = $this->db->count_all_results('user_salary');
                return $count;
            }

            if($column == 'last_row'){
                $last_row = $this->db->order_by('tax_id', 'desc')->limit(1)->get('user_salary')->row();
                return ($last_row);
            }

            $query = $this->db->select_sum($column)->get('user_salary')->row();
            return ($query->$column);
        }
    }
?>