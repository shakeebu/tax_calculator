<?php
    if (!defined('BASEPATH')) exit ('no direct script access allowed');

    if(!function_exists('tax_payable')) {
        function tax_payable($yearly_salary){
            $tax = 0;
            

            // switch statement to calculate tax on yearly salary
            switch ($yearly_salary) {
                
                // yearly salary less than 600,000
                case($yearly_salary<=600000):
                    $tax = 0;
                    break;

                // yearly salary is between 600,000 and 1,200,000
                case ($yearly_salary>600000 && $yearly_salary<=1200000):
                    $tax = (($yearly_salary - 600000) * (5/100));
                    break;
                
                // yearly salary is between 1,200,000 and 1,800,000
                case ($yearly_salary>1200000 && $yearly_salary<=1800000):
                    $tax = (($yearly_salary - 1200000) * (10/100)) + 30000;
                    break;

                // yearly salary is between 1,800,000 and 2,500,000
                case ($yearly_salary>1800000 && $yearly_salary<=2500000):
                    $tax = (($yearly_salary - 1800000) * (15/100)) + 90000;
                    break;

                // yearly salary is between 2,500,000 and 3,500,000
                case ($yearly_salary>2500000 && $yearly_salary<=3500000):
                    $tax = (($yearly_salary - 2500000) * (17.5/100)) + 195000;
                    break;

                // yearly salary is between 3,500,000 and 5,000,000
                case ($yearly_salary>3500000 && $yearly_salary<=5000000):
                    $tax = (($yearly_salary - 3500000) * (20/100)) + 370000;
                    break;

                // yearly salary is between 5,000,000 and 8,000,000
                case ($yearly_salary>5000000 && $yearly_salary<=8000000):
                    $tax = (($yearly_salary - 5000000) * (22.5/100)) + 670000;
                    break;

                // yearly salary is between 8,000,000 and 12,000,000
                case ($yearly_salary>8000000 && $yearly_salary<=12000000):
                    $tax = (($yearly_salary - 8000000) * (25/100)) + 1345000;
                    break;

                // yearly salary is between 12,000,000 and 30,000,000
                case ($yearly_salary>12000000 && $yearly_salary<=30000000):
                    $tax = (($yearly_salary - 12000000) * (27.5/100)) + 2345000;
                    break;

                // yearly salary is between 30,000,000 and 50,000,000
                case ($yearly_salary>30000000 && $yearly_salary<=50000000):
                    $tax = (($yearly_salary - 30000000) * (30/100)) + 7295000;
                    break;

                // yearly salary is between 50,000,000 and 75,000,000
                case ($yearly_salary>50000000 && $yearly_salary<=75000000):
                    $tax = (($yearly_salary - 50000000) * (32.5/100)) + 13295000;
                    break;

                // yearly salary is higher than 75,000,000
                default:
                $tax = (($yearly_salary - 75000000) * (35/100)) + 21420000;
                break;  

            }

            return $tax;
        }
    }
?>