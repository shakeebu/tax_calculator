

<div class="row py-3">
    <div class="col-sm-12">
        <h1>Tax Calculator Dashboard...</h1><hr>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-12">

                <!-- Profile Image -->
                <div class="card card-primary card-outline">
                    <div class="card-header text-center">
                        <h3>User Profile</h3>
                    </div>
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <img class="profile-user-img img-fluid img-circle"
                            src="<?php echo base_url();?>assets/dist/img/user4-128x128.jpg"
                            alt="User profile picture">
                        </div>

                        <h3 class="profile-username text-center"><?php echo strtoupper($user['name']) ?></h3>

                        <p class="text-muted text-center">ROLE: <?php echo $user['user_type'] == 1 ? "Admin" : "User" ?></p>

                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b><i class="fas fa-hashtag"></i> USER ID</b> <a class="float-right"><?php echo $user['id'] ?></a>
                            </li>
                            <li class="list-group-item">
                                <b><i class="fas fa-envelope"></i> EMAIL</b> <a class="float-right"><?php echo $user['email'] ?></a>
                            </li>
                            <li class="list-group-item">
                                <b><i class="fas fa-phone"></i> PHONE NO</b> <a class="float-right"><?php echo $user['phone'] ?></a>
                            </li>
                        </ul>

                        <a href="<?php echo base_url('user_controller/form_user/'.$user['id']); ?>" class="btn btn-primary btn-block edit-profile" id="<?php echo $user['id'] ?>"><b>Edit Profile</b></a>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->


                <!-- /.card -->
            </div>
            <!-- /.col -->
            <div class="col-lg-9 col-md-6 col-12">
                <!-- Small boxes (Stat box) -->
                <div class="row">

                    <div class="col-lg-6 col-12">
                        <div class="small-box p-3 bg-success">
                            <div class="inner my-5">
                                <h3><?php echo $user_count;?></h3>
                                <h5>User Registrations</h5>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person-add m-3" style="font-size:150px;"></i>
                            </div>
                            <a href="<?php echo base_url('user_controller/user_table'); ?>" class="small-box-footer mt-5">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->

                    <div class="col-lg-6 col-12">
                        <!-- small box -->
                        <div class="small-box p-3 bg-info">
                            <div class="inner my-5">
                                <h3><?php echo $salary_count;?></h3>
                                <h5>Salary Records</h5>
                            </div>
                            <div class="icon">
                                <i class="ion ion-pie-graph m-3" style="font-size:150px;"></i>
                            </div>
                            <a href="<?php echo base_url('salary_controller/salary_table?id='); ?>" class="small-box-footer mt-5">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                </div>
                    <!-- /.row -->
            </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>


