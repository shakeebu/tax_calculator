<?php
    $this->load->view("layout/header");
    $this->load->view('users/user_info');
?>


<div class="content-wrapper">
    <section class='content'>
        <div class="container-fluid">
            <div class="row py-3">
                <div class="col-sm-12">
                    <h1>Salary Form...</h1><hr>
                </div>
            </div>

            <div class="row">
                <div class="col-md-1"></div>

                <div class="col-md-10">
        
                    <div class='card'>

                        <div class='card-body'>
                                                                            
                            <form action="" method='post' id='add-update-salary'>

                                <div class="row">  
                                    <div class="col-md-1"></div> 
                                    <div class="col-md-10">   
                                                                    
                                        <input type='hidden' name='tax_id' id="tax_id" value="<?php echo $tax_id ?>">
                                        <input type='hidden' name='user_id' id="user_id" value="">

                                        <div class='form-group'>
                                            <label for="month_year">Month and Year:</label>
                                            <input type='date' class='form-control' name='month_year' id='month_year' placeholder='Enter the month' required>
                                        </div>

                                        <div class='form-group'>
                                            <label for="gross_salary">Gross Salary:</label>
                                            <input type='number' name='gross_salary' id='gross_salary' class='form-control' placeholder='Gross Salary' required>
                                        </div>

                                        <div class='form-group'>
                                            <label for="bonus">Bonus Amount:</label>
                                            <input type='number' name='bonus' id='bonus' class='form-control' placeholder='Bonus Amount'>
                                        </div>

                                        <div class='form-group'>
                                            <label for="bonus_type">Reason for Bonus:</label>
                                            <input type='text' name='bonus_type' id='bonus_type' class='form-control' placeholder='Reason for Bonus'>
                                        </div>

                                        <div class='form-group'>
                                            <label for="medical_allowance">Medical Allowance:</label>
                                            <input type='number' name='medical_allowance' id='medical_allowance' class='form-control' placeholder='Medical Allowance' required>
                                        </div>

                                        <div class='form-group'>
                                            <label for="medical_allowance"> Tax Paid:</label>
                                            <input type='number' name='tax_paid' id='tax_paid' class='form-control' placeholder='Tax Paid' required>
                                        </div>

                                        <div class="mt-3">
                                            <a href="<?php echo base_url('salary_controller/salary_table?id='); ?>" class='btn btn-outline-secondary ml-1 float-right'><i class='fa fa-ban'></i> Cancel </a>
                                            <button type='submit' id='' class='btn btn-outline-success ml-1 float-right'><i class='fas fa-save'></i> Save & Close </button> 
                                            <button type='submit' id='button-submit' class='btn btn-outline-success ml-1 float-right'><i class='fas fa-save'></i> Save </button>
                                        </div>
                                    </div>
                                    <div class="col-md-1"></div>
                                </div>    
                            </form>    

                        </div>
                    </div><!-- /.card -->
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
    </section>
</div>



<?php $this->load->view('layout/footer'); ?>

<script>

    // on ready get to know if there is something in #tax_id, we are trying to edit record else we are adding record
    $(document).ready(function(){
        var session_id = <?php echo $this->session->userdata('userId'); ?>;
        var tax_id = $("#tax_id").val();
        // if tax id existis fetch data against that tax id and put them into form for edit 
        if(tax_id) {
            $.ajax({
                url:"<?php echo base_url('salary_controller/fetch_data'); ?>",
                method:"POST",
                data:{tax_id:tax_id},
                datatype:"Json",
                success:function(data){
                    var decode = JSON.parse(data);
                    // put received data in form to edit 
                    $('#tax_id').attr("value",decode.tax_id);
                    $('#user_id').attr("value",decode.user_id);
                    $('#month_year').attr("value",decode.month_year);
                    $('#gross_salary').attr("value",decode.gross_salary);
                    $('#bonus').attr("value",decode.bonus);
                    $('#bonus_type').attr("value", decode.bonus_type);
                    $('#medical_allowance').attr("value",decode.medical_allowance);
                    $('#tax_paid').attr("value",decode.tax_paid);
                }
            })
        } else {
            // else set user id value of session id so that a new tax record can get a user id 
            $('#user_id').attr("value", session_id);
        }
    });

    // on submit form after entering records
    $('#add-update-salary').on('submit', function(event){
        event.preventDefault();
        var session_id = <?php echo $this->session->userdata('userId'); ?>;
        var session_type = <?php echo $this->session->userdata('user_type')?>;
        var tax_id = $('#tax_id').val();
        $.ajax({
            url:"<?php echo base_url();?>salary_controller/add_update_salary/"+tax_id,
            method:'POST',
            data:$('#add-update-salary').serialize(),
            datatype:'Json',
            success: function(details){
                // if submit button is clicked just show the toaster
                if($(document.activeElement).attr('id') == "button-submit"){
                    tax_id == '' ? toasterAlert('success', 'Salary Record Added') : toasterAlert('success', 'Salary Record Updated');
                } else {
                    // else if save and close is clicked redirect to table again after saving in db
                    // if usertype is user redirect him to salary table containig only his records
                    if (session_type != 1) {
                        window.location.href = "<?php echo base_url();?>salary_controller/salary_table?id="+session_id;
                    } else {
                        // if user is admin he will be redirected to page containing each and every record
                        window.location.href = "<?php echo base_url();?>salary_controller/salary_table?id=";
                    }
                    
                }
            }

        })
    })
</script>