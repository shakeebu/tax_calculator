<body class="hold-transition register-page">
<?php $this->load->view("layout/header"); ?>       
    <!-- Status message -->
    <?php  
        if(!empty($success_msg)){ 
            echo '<p class="status-msg success">'.$success_msg.'</p>'; 
        }elseif(!empty($error_msg)){ 
            echo '<p class="status-msg error">'.$error_msg.'</p>'; 
        } 
    ?>
        
<div class="register-box">
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <a href="" class="h1"><b>TAX</b>calculator</a>
    </div>
    <div class="card-body">

      <form action="<?= base_url(); ?>user_controller/register_user" method="post">

        <div class="input-group mb-3">
          <input type="text" class="form-control" name="name" id='name' placeholder="Full Name" value="<?php echo !empty($user['name']) ? $user['name'] : ''; ?>">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <?php echo form_error('name','<p class="help-block">','</p>'); ?>
        <div class="input-group mb-3">
          <input type="email" name="email" class="form-control" placeholder="Email" value="<?php echo !empty($user['email'])?$user['email']:''; ?>" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <?php echo form_error('email','<p class="help-block">','</p>'); ?>
        <div class="input-group mb-3">
          <input type="tel" name="phone" pattern='[0]{1}[3]{1}[0-4]{1}[0-9]{8}' class="form-control" placeholder="Phone Number" value="<?php echo !empty($user['phone'])?$user['phone']:''; ?>" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-phone"></span>
            </div>
          </div>
        </div>
        <?php echo form_error('phone','<p class="help-block">','</p>'); ?>
        <div class="input-group mb-3">
          <input type="password" name="password" class="form-control" placeholder="Password" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <?php echo form_error('password','<p class="help-block">','</p>'); ?>
        <div class="input-group mb-3">
          <input type="password" name="confirm_password" class="form-control" placeholder="Retype password" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <?php echo form_error('confirm_password','<p class="help-block">','</p>'); ?>
        <div class="row">
          <div class="col-8">
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block" name="signupSubmit" value="CREATE ACCOUNT">REGISTER</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

        
      <p>Already have an account? <a href="<?php echo base_url('login'); ?>">Login here</a></p>
      
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->
<?php $this->load->view("layout/footer"); ?>



<script>
  
  $('#name').on('focusout', function(){
     var str = $.trim($(this).val().toString());
     removeSpaces = str.replace(/[ ][ ]*/g, ' ');
     $(this).val(removeSpaces);
  })
</script>