<?php
    $this->load->view('layout/header');
    $this->load->view('users/user_info');
?>

<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row py-3">
                <div class="col-sm-12">
                    <h1>Salary Records...</h1><hr>
                </div>
            </div>

            <div class="row">

                <div class= 'col-3'>

                    <div class="card card-outline">
                        <div class="card-header bg-dark text-center">
                            <h3>Yearly Tax Predictions...</h3>
                        </div>
                    <div class="card-body box-profile">
                        <div class="row border-bottom pb-2">
                            <div class="col-5">
                                <input class='col-12' type="number" min="2015" max="2025" placeholder="Year" id="start_year">
                            </div>
                            <div class="col-1 text-center p-1">
                                <i class="fa fa-window-minimize" aria-hidden="true"></i>
                            </div>
                            <div class="col-5">
                                <input class="col-12" type="number" placeholder="Year" id="end_year" disabled>
                            </div>
                            <div class="col-1 text-center p-1" id="search">
                                <i class="fa fa-search" aria-hidden="true"></i>
                            </div>
                            <input type="hidden" id="my_id" value="<?php echo $this->input->get('id') ?>">
                        </div>

                        <div class="row">
                            <table class="col-12 mt-4 text-center table-bordered table-hover">
                                <tr>
                                    <th class='bg-dark p-2'>Total Taxable Amount</th>
                                </tr>
                                <tr>
                                    <td class='p-2' id="total_salary">Total Taxable Amount</td>
                                </tr>
                                <tr>
                                    <th class='bg-dark p-2'>Tax Paid</th>
                                </tr>
                                <tr>
                                    <td class='p-2' id='tax_paid'>Tax Paid</td>
                                </tr>
                                <tr>
                                    <th class='bg-dark p-2'>Expected Yearly Tax</th>
                                </tr>
                                <tr>
                                    <td class='p-2' id='expected_tax'>Expected Yearly Tax</td>
                                </tr>
                                <tr>
                                    <th class='bg-dark p-2'>Expected Remaining Tax per Month</th>
                                </tr>
                                <tr>
                                    <td class='p-2'id='tax_per_month'>Tax per Month</td>
                                </tr>

                            </table>
                        </div>
                            
                    </div>
                    <!-- /.card-body -->
                </div>
                </div>

                <div class="col-9">
                    
                    <div class="card">

                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>TAX ID</th>
                                        <th>USER ID </th>
                                        <th>DATE</th>
                                        <th>GROSS SALARY</th>
                                        <th>BONUS</th>
                                        <th>BONUS REASON</th>
                                        <th>MEDICAL ALLOWNACE</th>
                                        <th>Tax Payable Amount</th>
                                        <th>TAX PAID</th>
                                        <th>ACTIONS</th>
                                    </tr>
                                </thead>

                                <tbody>  
                                    <?php 
                                        foreach($records as $row) {
                                            echo "<tr id='row".$row->tax_id."'><td>". $row->tax_id . "</td>".
                                                "<td>". $row->user_id . "</td>".
                                                "<td>". $row->month_year . "</td>".
                                                "<td>". $row->gross_salary . "</td>".
                                                "<td>". $row->bonus . "</td>".
                                                "<td>". $row->bonus_type . "</td>".
                                                "<td>". $row->medical_allowance . "</td>".
                                                "<td>". $row->tax_payable_amount . "</td>".
                                                "<td>". $row->tax_paid . "</td>".
                                                "<td><a href='".base_url()."salary_controller/form_salary/".$row->tax_id."'> <button type='button' id='".$row->tax_id."'  class='btn btn-outline-primary btn-sm edit-salary'><i class='fas fa-edit'></i></button></a>
                                                <button type='button' id='".$row->tax_id."' class='btn btn-outline-danger btn-sm delete-salary'><i class='fa fa-trash'></i></button></td></tr>";
                                            }
                                    ?>
                    
                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>
</div>

<?php
    $this->load->view('users/delete_modal');
    $this->load->view('layout/footer');
?>

<script>
    $('#nav-view-salary').addClass('active');


    $(document).on('click', '.delete-salary', function(){
        var id = $(this).attr('id');
        $('#delete-id').val(id);
        $('#modal-delete').modal('show');
    });

    // delete a record on click
    $(document).on('click', '#delete', function(){
        var id = $('#delete-id').val();
        $.ajax({
            url:"<?php echo base_url();?>salary_controller/delete_salary/"+id, 
            type:"GET",
            data:{id:id},
            datatype:"Json",
            success:function(details){
                $('#row'+id).remove();
                $('#modal-delete').modal('hide');
                toasterAlert('error', 'Salary Deleted');
            }
        })
    });

    // focusout year input function
    $('#start_year').on('focusout', function(){
        // if invalid year border color red
     if ($('#start_year').val() < 2015 ||  $('#start_year').val() > 2025) {
        $('#start_year').css("border",'2px solid red');
     } else {
        //  if valid year border color black and set end year value
        $('#start_year').css("border",'2px solid black');
        $('#end_year').val(parseInt($('#start_year').val())+1);
        }
    });

    // search button clicked
    $('#search').on('click', function(){
        // if valid year 
        if($('#start_year').val() >= 2015 &&  $('#start_year').val() <= 2025){
            let id = $('#my_id').val();
            let start_date = $('#start_year').val();
            let end_date = $('#end_year').val();
            $.ajax({
                // get data to put in expected salary table
                url:'<?php echo base_url();?>salary_controller/expected_tax?id='+id+'&start_year='+start_date+'&end_year='+end_date,
                type:"POST",
                datatype:'Json',
                success:function(details){
                    var decode = JSON.parse(details);
                        $('#total_salary').text(decode.taxable_amount ? decode.taxable_amount : 0);
                        $('#tax_paid').text(decode.tax_paid ? decode.tax_paid : 0);
                        // if specific user we can predict yearly tax, for all records we can't
                        if (id) {
                            $('#expected_tax').text(decode.expected_tax);
                            $('#tax_per_month').text(decode.tax_per_month);
                        }

                }
            })
        } else {
            alert('Please enter an year between 2015 and 2025');
        }
    });

</script>