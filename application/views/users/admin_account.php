<?php 
    $this->load->view('layout/header');
    $this->load->view('users/user_info');
?>

<div class="content-wrapper">

    <section class="content">

        <div class="container-fluid" id="dashboard">
            <?php $this->load->view('layout/dashboard'); ?>
        </div>
    </section>

</div>


<?php   
    $this->load->view('users/delete_modal');
    $this->load->view('layout/footer'); ?>

</body>

