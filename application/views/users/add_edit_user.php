<?php
    $this->load->view("layout/header");
    $this->load->view('users/user_info');
?>


<div class="content-wrapper">
    <section class='content'>
        <div class="container-fluid">
            <div class="row py-3">
                <div class="col-sm-12">
                    <h1>User Form...</h1><hr>
                </div>
            </div>

            <div class="row">
                <div class="col-md-1"></div>

                <div class="col-md-10">
        
                    <div class='card'>

                        <div class='card-body'>
                                                                            
                            <form action='' method='post' id="insert_form">

                                <input type='hidden' id='id' name='id' value="<?php echo $id ?>">

                                <div class="form-group row">
                                    <div class="col-md-1"></div>
                                    <label for="name" class="col-md-2">User Name:</label>
                                    <div class='input-group mb-3 col-md-8'>
                                        <input type='text' class='form-control' name='name' id='name' placeholder='Full Name' value='' required>
                                        <div class='input-group-append'>
                                            <div class='input-group-text'>
                                                <span class='fas fa-user'></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1"></div>
                                </div>


                                <div class="form-group row">
                                    <div class="col-md-1"></div>
                                    <label for="name" class="col-md-2">Email Address:</label>
                                    <div class='input-group mb-3 col-md-8'>
                                        <input type='email' name='email' id='email' class='form-control' placeholder='Email' value='' required>
                                        <div class='input-group-append'>
                                            <div class='input-group-text'>
                                                <span class='fas fa-envelope'></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1"></div>
                                </div>
                                                

                                <div class="form-group row">
                                    <div class="col-md-1"></div>
                                    <label for="name" class="col-md-2">Phone Number:</label>
                                    <div class='input-group mb-3 col-md-8'>
                                        <input type='tel' pattern='[0]{1}[3]{1}[0-4]{1}[0-9]{8}' name='phone' id='phone' class='form-control' placeholder='Phone Number' value='' required>
                                        <div class='input-group-append'>
                                            <div class='input-group-text'>
                                                <span class='fas fa-phone'></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1"></div>
                                </div>          

                                <div class="form-group row">
                                    <div class="col-md-1"></div>
                                    <label for="user-type" class="col-md-2">User Type:</label>
                                    <div class='input-group mb-3 col-md-8'>
                                        <select class='form-control select2' name='user_type' id='user_type'>
                                            <option value='user'>User</option>
                                            <option value='admin'>Admin</option>
                                        </select>
                                        <div class='input-group-append'>
                                            <div class='input-group-text'>
                                                <span class='fas fa-user-tag'></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1"></div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-1"></div>
                                    <label for="name" class="col-md-2">Password:</label>
                                    <div class='input-group mb-3 col-md-8'>
                                        <input type='password' name='password' id='password' class='form-control' placeholder='Password' required>
                                        <div class='input-group-append'>
                                            <div class='input-group-text'>
                                                <span class='fas fa-lock'></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1"></div>
                                </div>   
                                    
                                <div class="row">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-10">
                                        <a href="<?php echo base_url('user_controller/user_table'); ?>" class='btn btn-outline-secondary ml-1 float-right'><i class='fa fa-ban'></i> Cancel </a>
                                        <button type='submit' id='' class='btn btn-outline-success ml-1 float-right'><i class='fas fa-save'></i> Save & Close </button> 
                                        <button type='submit' id='button-submit' class='btn btn-outline-success ml-1 float-right'><i class='fas fa-save'></i> Save </button>
                                    </div>
                                    <div class="col-md-1"></div>
                                </div>
                                    
                            </form>    

                        </div>
                    </div><!-- /.card -->
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
    </section>
</div>



<?php $this->load->view('layout/footer'); ?>

<script>
    $('#nav-add-user').addClass('active');
    // function to validate if name contain only spaces or have many spaces
    $('#name').on('focusout', function(){
     var str = $.trim($(this).val().toString());
     removeSpaces = str.replace(/[ ][ ]*/g, ' ');
     $(this).val(removeSpaces);
    })

    $(document).ready(function(){
        // check if #id has value, if yes fetch data to edit and put it in form 
        var id = $("#id").val();
        if(id) {
            $.ajax({
                url:"<?php echo base_url('user_controller/fetch_data'); ?>",
                method:"POST",
                data:{id:id},
                datatype:"Json",
                success:function(data){
                    // put fetched data in form
                    var decode = JSON.parse(data);
                    $('#id').attr("value",decode.id);
                    $('#name').attr("value",decode.name);
                    $('#email').attr("value",decode.email);
                    $('#phone').attr("value",decode.phone);
                    // $('#password').attr("value",decode.password);
                    $(`#user_type option[value='${decode.user_type == 1 ? 'admin' : 'user'}']`).prop('selected', true);
                }
            })
        }
    });

    // function for submitting form
    $('#insert_form').on('submit', function(event){
        event.preventDefault();
        var id = $('#id').val();
        $.ajax({
            url:"<?php echo base_url();?>user_controller/update_user/"+id,
            method:"POST",
            data:$('#insert_form').serialize(),
            datatype:"Json",
            success:function(data){
                // if clicked button is save then show toaster and if clicked save and close redirect him to user table
                if($(document.activeElement).attr('id') == "button-submit"){
                    id == '' ? toasterAlert('success', 'New User Added') : toasterAlert('success', 'User Record Edited');
                } else {
                    window.location.href = "<?php echo base_url();?>user_controller/user_table";
                }
            }  
        })
    })

    $('.select2').select2();
</script>