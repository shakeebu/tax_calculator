<?php 
    $this->load->view('layout/header');
    $this->load->view('users/user_info');
?>

<div class="content-wrapper">

    <section class="content">

        <div class="container-fluid" id="">
            
            <div class="row py-3">
                <div class="col-sm-12">
                    <h1>Registered Users...</h1><hr>
                </div>
            </div>

            <div class="row">

                <div class="col-12">
                
                    <div class="card">

                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>ID</th>
                                        <th>FULL NAME</th>
                                        <th>EMAIL</th>
                                        <th>PHONE NUMBER</th>
                                        <th>USER ROLE</th>
                                        <th>ACTIONS</th>
                                    </tr>
                                </thead>

                                <tbody>  
                                    <?php 
                                        foreach($records as $row) {
                                            echo "<tr id='row".$row->id."'><td>". $row->id . "</td>".
                                                "<td>". $row->name . "</td>".
                                                "<td>". $row->email . "</td>".
                                                "<td>". $row->phone . "</td>".
                                                "<td>". ($row->user_type == 1 ? "admin" : "user") . "</td>".
                                                "<td><a href='".base_url()."salary_controller/salary_table?id=".$row->id."'><button type='button' id='".$row->id."'  class='btn btn-outline-success btn-sm view-user'><i class='fas fa-eye'></i></button></a>
                                                <a href='".base_url()."user_controller/form_user/".$row->id."'><button type='button' id='".$row->id."'  class='btn btn-outline-primary btn-sm edit-user'><i class='fas fa-edit'></i></button></a>
                                                <button type='button' id='".$row->id."' class='btn btn-outline-danger btn-sm delete-user'><i class='fa fa-trash'></i></button> </td></tr>";
                                        }
                                    ?>
                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
            <!-- /.card -->
            </div>
        </div>
    </section>
</div>


<?php   
    $this->load->view('users/delete_modal');
    $this->load->view('layout/footer'); ?>

<script>

    $('#nav-view-user').addClass('active');

    // // edit user button click
    // $(document).on('click', '.edit-user', function(){
    //     var id = $(this).attr('id');
        
    //     $.ajax({
    //         // fetch data for user and put into edit form
    //         url:"<?php echo base_url('user_controller/fetch_data'); ?>",
    //         method:"POST",
    //         data:{id:id},
    //         datatype:"Json",
    //         success:function(details){
    //             var decode = JSON.parse(details);
    //             $('#id').attr("value",decode.id);
    //             $('#name').attr("value",decode.name);
    //             $('#email').attr("value",decode.email);
    //             $('#phone').attr("value",decode.phone);
    //             $('#password').attr("value",decode.password);
    //             $('#user_type').attr("value",decode.user_type == 1 ? 'admin' : 'user');  
    //         }

    //     })
    // });

    // // on submit update user form
    // $(this).on('submit', function(event){
    //     event.preventDefault();
    //     var id = $('#id').val();
    //     $.ajax({
    //         // update data in database and show in table
    //         url:"/tax_calculator/user_controller/update_user/"+id,
    //         method:"POST",
    //         data:$('#insert_form').serialize(),
    //         datatype:"Json",
    //         success:function(data){ 
    //             $('#modal-edit').modal('hide');
    //             var decode = JSON.parse(data);    
    //             if($("#row" + decode.id).length == 0) {
    //                 $("#example2 > tbody").append("<tr id='row"+decode.id+"'></tr>");
    //             } 
    //             $('#row'+decode.id).html("<td>"+decode.id+"</td>"+
    //                             "<td>"+decode.name+"</td>"+
    //                             "<td>"+decode.email+"</td>"+
    //                             "<td>"+decode.phone+"</td>"+
    //                             "<td>"+(decode.user_type == 1 ? 'admin' : 'user')+"</td>"+
    //                             "<td>"+decode.password+"</td>"+
    //                             "<td><button type='button' id='"+decode.id+"' class='btn btn-outline-success btn-sm view-user'><i class='fas fa-eye'></i></button>"+
    //                             " <button type='button' id='"+decode.id+"' class='btn btn-outline-primary btn-sm edit-user'><i class='fas fa-edit'></i></button>"+
    //                             " <button type='button' id='"+decode.id+"' class='btn btn-outline-danger btn-sm delete'><i class='fa fa-trash'></i></button></td>"
    //                         );

    //             id == '' ? toasterAlert('success', 'New User Added') : toasterAlert('success', 'User Record Edited');
    //         }  

    //     })
    // });


    $(document).on('click', '.delete-user', function(){
        var id = $(this).attr('id');
        $('#delete-id').val(id);
        $('#modal-delete').modal('show');
    });

    // delete button function
    $(document).on('click', '#delete', function(){ 
        var id = $('#delete-id').val();
        $.ajax({
            url:"<?php echo base_url();?>user_controller/delete_user/"+id, 
            type:"GET",
            data:{id:id},
            datatype:"Json",
            success:function(details){
                $('#modal-delete').modal('hide');
                $('#row'+id).remove();
                toasterAlert('error', 'User Deleted');
            }
        })
    });
    
</script>
</body>

