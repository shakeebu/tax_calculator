<body class="hold-transition sidebar-mini layout-fixed">

<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar main-sidebar-custom sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo base_url('profile'); ?>" class="brand-link">
      <img src="<?php echo base_url();?>assets/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"><b id='dash'>Tax Calculator </b></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo base_url();?>assets/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="<?php echo base_url('profile'); ?>" class="d-block" id="userNameHead"> <?php echo strtoupper($this->session->userdata('userName'));?></a>
        </div>
      </div>

    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item menu-open border-bottom mb-2 pb-2" id="nav_users">
            <a href="#" class="nav-link active">
            <i class="fa fa-users nav-icon"></i>
              <p>
                Users
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url('user_controller/user_table'); ?>" class="nav-link" id='nav-view-user'>
                  <i class="fa fa-table nav-icon"></i>
                  <p>View Users</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="<?php echo base_url('user_controller/form_user'); ?>" class="nav-link" id='nav-add-user'>
                  <i class="fa fa-plus-circle nav-icon"></i>
                  <p>Add User</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item menu-open">
            <a href="#" class="nav-link active">
              <i class="fa fa-university nav-icon"></i>
              <p>
                Salary
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url('salary_controller/salary_table?id='); ?>" class="nav-link" id='nav-view-salary'>
                  <i class="fa fa-table nav-icon"></i>
                  <p>View Salary</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url('salary_controller/form_salary'); ?>" class="nav-link" id='nav-add-user'>
                  <i class="fa fa-plus-circle nav-icon"></i>
                  <p>Add Salary</p>
                </a>
              </li>
            </ul>
          </li>
</ul>
    </nav>
    
    </div>
    <!-- /.sidebar -->

    <div class="sidebar-custom">
      <a href="<?php echo base_url('user_controller/logout'); ?>" class="btn btn-secondary hide-on-collapse pos-right">LOGOUT</a>
    </div>
    <!-- /.sidebar-custom -->
  </aside>
